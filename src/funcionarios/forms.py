from django import forms

from .models import Funcionario


class FuncionarioForm(forms.ModelForm):
    class Meta:
        model = Funcionario
        fields = ['run','nombres', 'primer_apellido', 'segundo_apellido', 'fecha_nacimiento', 'email', 'direccion',
                  'telefono', 'profesion', 'feriado_legal', 'dias_administrativos', 'numero_registro','activo']
        widgets = {
            'run': forms.TextInput(attrs={'class': 'ui small input'}),
            'nombres': forms.TextInput(attrs={'class': 'ui small input'}),
            'primer_apellido': forms.TextInput(attrs={'class': 'ui small input'}),
            'segundo_apellido': forms.TextInput(attrs={'class': 'ui small input'}),
            'fecha_nacimiento': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'email': forms.EmailInput(attrs={'class': 'ui small input'}),
            'direccion': forms.TextInput(attrs={'class': 'ui small input'}),
            'telefono': forms.TextInput(attrs={'class': 'ui small input'}),
            'profesion': forms.Select(attrs={'class': 'ui mini selection dropdown'}),
            'feriado_legal': forms.NumberInput(attrs={'class': 'ui small input'}),
            'dias_administrativos': forms.NumberInput(attrs={'class': 'ui small input'}),
            'numero_registro': forms.TextInput(attrs={'class': 'ui small input'}),
            'activo': forms.CheckboxInput(attrs={'class': 'ui checkbox'}),
        }


    def clean_run(self):
        run = self.cleaned_data.get('run')
        if Funcionario.objects.filter(run=run).exists():
            raise forms.ValidationError("El RUN ya esta registrado en la Base de Datos")
        return run

class FuncionarioFormUpdate(forms.ModelForm):
    class Meta:
        model = Funcionario
        fields = ['nombres', 'primer_apellido', 'segundo_apellido', 'fecha_nacimiento', 'email', 'direccion',
                  'telefono', 'profesion', 'feriado_legal', 'dias_administrativos', 'numero_registro','activo']
        widgets = {
            'nombres': forms.TextInput(attrs={'class': 'ui small input'}),
            'primer_apellido': forms.TextInput(attrs={'class': 'ui small input'}),
            'segundo_apellido': forms.TextInput(attrs={'class': 'ui small input'}),
            'fecha_nacimiento': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'email': forms.EmailInput(attrs={'class': 'ui small input'}),
            'direccion': forms.TextInput(attrs={'class': 'ui small input'}),
            'telefono': forms.TextInput(attrs={'class': 'ui small input'}),
            'profesion': forms.Select(attrs={'class': 'ui mini selection dropdown'}),
            'feriado_legal': forms.NumberInput(attrs={'class': 'ui small input'}),
            'dias_administrativos': forms.NumberInput(attrs={'class': 'ui small input'}),
            'numero_registro': forms.TextInput(attrs={'class': 'ui small input'}),
            'activo': forms.CheckboxInput(attrs={'class': 'ui checkbox'}),
        }
