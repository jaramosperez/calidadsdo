from django.db import models
from profesiones.models import Profesion
from .managers import FuncionarioManager

class Funcionario(models.Model):
    """
    Clase para crear Funcionarios con el objectivo de asignarlo en:
    Documentos, características o ámbitos
    """
    run = models.CharField(
        max_length=11, 
        verbose_name="RUN",
        unique=True,
        )
    nombres = models.CharField(
        max_length=255, verbose_name="Nombres"
        )
    primer_apellido = models.CharField(
        max_length=50, verbose_name="Primer apellido"
        )
    segundo_apellido = models.CharField(
        max_length=50, verbose_name="Segundo apellido", blank=True
        )
    fecha_nacimiento = models.DateField(
        verbose_name="Fecha de nacimiento",
        blank=True,
        null=True)
    email = models.EmailField(verbose_name="Email", blank=True, null=True)
    telefono = models.BigIntegerField(
        verbose_name="Número de teléfono", 
        blank=True,
        null=True)
    direccion = models.CharField(
        max_length=255,
        verbose_name="Dirección",
        blank=True,
        null=True
    )
    profesion = models.ForeignKey(
        Profesion, 
        on_delete=models.CASCADE, 
        verbose_name="Profesion")
    dias_administrativos = models.SmallIntegerField(
        default=0,
        verbose_name="Cantidad de días administrativos"
        )
    feriado_legal = models.SmallIntegerField(
        default=0,
        verbose_name="Cantidad de feriados legales"
    )
    activo = models.BooleanField(
        default=True,
        verbose_name="Esta activo")
    numero_registro = models.IntegerField(verbose_name="Número de registro", blank=True, null=True)
    created = models.DateTimeField(
        auto_now_add=True, verbose_name='Fecha de creación'
        )
    updated = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de edición'
        )

    objects = FuncionarioManager()
    
    class Meta:
        verbose_name = 'Funcionario'
        verbose_name_plural = 'Funcionarios'

    def __str__(self):
        nombre_completo = self.nombres + self.primer_apellido
        if self.segundo_apellido != '':
            nombre_completo = self.nombres + ' ' + self.primer_apellido + ' ' + self.segundo_apellido
        return nombre_completo
