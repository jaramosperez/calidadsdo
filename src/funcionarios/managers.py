from django.db import models


class FuncionarioManager(models.Manager):
    """ Manager para listar solamente Indicadores """

    def listado_funcionarios(self):
        
        return self.all()

    def nombre_completo(self):
        nombre_completo = self.nombres + self.primer_apellido
        if self.segundo_apellido != '':
            nombre_completo = self.nombres + ' ' + self.primer_apellido + ' ' + self.segundo_apellido
        return nombre_completo