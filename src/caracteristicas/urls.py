from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required
from .views import CaracteristicaList, CaracteristicaDetail
from .views import CaracteristicaUpdate

caracteristicas_patterns = ([
    path('', login_required(CaracteristicaList.as_view()), name='caracteristicas'),
    path(
        '<int:pk>/<slug:slug>/',
        login_required(CaracteristicaDetail.as_view()),
        name='caracteristica'),
    path(
        'actualizar/<int:pk>/<slug:slug>/',
        login_required(CaracteristicaUpdate.as_view()), name='actualizar'
        ),
], 'caracteristicas')
