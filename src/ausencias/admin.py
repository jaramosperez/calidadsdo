from django.contrib import admin
from .models import Ausencia, TipoAusencia, TipoJornada


class AusenciaAdmin(admin.ModelAdmin):
    """
    Clase para usar el Modelo Ambito en el DjangoAdmin.
    """

class TipoAusenciaAdmin(admin.ModelAdmin):
    """
    Clase para usar el Modelo Tipo de Ausencia
    """

class TipoJornadaAdmin(admin.ModelAdmin):
    """
    Clase para usar el Modelo Tipo de Jornada
    """


admin.site.register(Ausencia, AusenciaAdmin)
admin.site.register(TipoAusencia, TipoAusenciaAdmin)
admin.site.register(TipoJornada, TipoJornadaAdmin)
admin.site.empty_value_display = '(None)'
