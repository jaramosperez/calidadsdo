from django.db import models
from funcionarios.models import Funcionario

# Create your models here.
class TipoAusencia(models.Model):
    nombre = models.CharField(max_length=50, verbose_name="Nombre Ausencia")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name='Fecha de creación'
        )
    updated = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de edición'
        )
    
    class Meta:
        verbose_name = "Tipo de Ausencia"
        verbose_name_plural = "Tipos de Ausencias"
    
    def __str__(self):
        return self.nombre


class TipoJornada(models.Model):
    nombre = models.CharField(max_length=50, verbose_name="Nombre de la jornada")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name='Fecha de creación'
        )
    updated = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de edición'
        )
    
    class Meta:
        verbose_name = "Tipo de Jornada"
        verbose_name_plural = "Tipos de Jornadas"

    def __str__(self):
        return self.nombre


class Ausencia(models.Model):
    tipo_ausencia = models.ForeignKey(
        TipoAusencia,
        verbose_name="Tipo de Ausencia",
        on_delete=models.CASCADE
        )
    funcionario = models.ForeignKey(
        Funcionario,
        on_delete=models.CASCADE,
        verbose_name="Funcionario",
        related_name='ausencia_funcionario'
    )
    fecha_inicio = models.DateField(verbose_name="Fecha de Inicio")
    fecha_termino = models.DateField(verbose_name="Fecha de Término")
    tipo_jornada = models.ForeignKey(
        TipoJornada,
        verbose_name="Tipo de Jornada",
        on_delete=models.CASCADE
    )
    class Meta:
        verbose_name = "Ausencia"
        verbose_name_plural = "Ausencias"

    def __str__(self):
        return f"Ausencia de {self.funcionario.nombre}"
