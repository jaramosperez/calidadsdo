from django import forms
from .models import Ausencia


class AusenciaForm(forms.ModelForm):
    class Meta:
        model = Ausencia
        fields = ['tipo_ausencia', 'fecha_inicio', 'fecha_termino', 'tipo_jornada']
        widgets = {
            'tipo_ausencia': forms.Select(attrs={'class': 'ui small selection dropdown'}),
            'fecha_inicio': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'fecha_termino': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'tipo_jornada': forms.Select(attrs={'class': 'ui small selection dropdown'}),
        }


class AusenciaUpdateForm(forms.ModelForm):
    class Meta:
        model = Ausencia
        fields = ['tipo_ausencia', 'fecha_inicio', 'fecha_termino', 'tipo_jornada']
        widgets = {
            'tipo_ausencia': forms.Select(attrs={'class': 'ui small selection dropdown'}),
            'fecha_inicio': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'fecha_termino': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'tipo_jornada': forms.Select(attrs={'class': 'ui small selection dropdown'}),
        }