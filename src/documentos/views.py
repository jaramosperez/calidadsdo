from .models import Documento
from caracteristicas.models import Caracteristica
from django.urls import reverse_lazy
from .forms import DocumentoForm
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required


@method_decorator(login_required(), name='dispatch')
class DocumentoList(ListView):
    """
    Clase para listar solo los indicadores.
    """

    def get_queryset(self):
        return Documento.objects.listado_indicadores()


@method_decorator(login_required(), name='dispatch')
class DocumentoCreate(CreateView):
    """
    Clase para ingresar documentos al sistema.
    No Tiene Filtro de Archivos
    [TODO]
    Crear un filtro para tipo de archivos.
    """
    model = Documento
    form_class = DocumentoForm
    success_url = reverse_lazy('documentos:documentos')

    # IDENTIFICAR AL FUNCIONARIO AL QUE SE LE AÑADIRÁ LA AUSENCIA.
    def form_valid(self, form):
        caracteristica_id = self.kwargs['caracteristica_id']
        form.instance.caracteristica = Caracteristica.objects.get(id=caracteristica_id)
        return super(DocumentoCreate, self).form_valid(form)

    # OBTENER LOS DATOS PARA EL FUNCIONARIO SELECCIONADO.
    def get_context_data(self, **kwargs):
        context = super(DocumentoCreate, self).get_context_data(**kwargs)
        caracteristica_id = self.kwargs['caracteristica_id']
        caracteristica = Caracteristica.objects.get(id=caracteristica_id)
        context['caracteristica'] = caracteristica
        return context
