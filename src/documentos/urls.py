from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required
from .views import DocumentoList, DocumentoCreate

documentos_patterns = ([
    path('', (DocumentoList.as_view()), name='documentos'),
    path('crear/<caracteristica_id>', (DocumentoCreate.as_view()), name="crear"),
], 'documentos')
