from django.db import models


class IndicadoresManager(models.Manager):
    """ Manager para listar solamente Indicadores """

    def listado_indicadores(self):
        return self.filter(tipo_documento__nombre='Indicador')