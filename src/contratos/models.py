from django.db import models
from funcionarios.models import Funcionario
from .managers import ContratoManager

# Create your models here.
class TipoContrato(models.Model):
    nombre = models.CharField(
        max_length=20, 
        verbose_name="Nombre del tipo de Contrato")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name='Fecha de creación'
        )
    updated = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de edición'
        )    


    class Meta:
        verbose_name = 'Tipo de contrato'
        verbose_name_plural = 'Tipos de contratos'

    def __str__(self):
        return self.nombre


class Contrato(models.Model):
    funcionario = models.ForeignKey(
        Funcionario,
        on_delete=models.CASCADE,
        verbose_name="Funcionario",
        related_name="contrato_funcionario")
    fecha_inicio = models.DateField(
        verbose_name="Fecha de inicio contrato"
    )
    fecha_termino = models.DateField(verbose_name="Fecha de término de contrato")
    fecha_induccion = models.DateField(
        verbose_name="Fecha de inducción",
        blank=True,
        null=True)
    corresponde_induccion = models.BooleanField(
        default=True, 
        verbose_name="Le Corresponde Inducción")
    tipo_contrato = models.ForeignKey(
        TipoContrato, 
        on_delete=models.CASCADE,
        verbose_name="Tipo de Contrato")
    vigente = models.BooleanField(
        default=True,
        verbose_name="Contrato Vigente"
    )
    permanente = models.BooleanField(
        verbose_name="Permanente",
        default=True
    )
    created = models.DateTimeField(
        auto_now_add=True, verbose_name='Fecha de creación'
        )
    updated = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de edición'
        )
    
    objects = ContratoManager()
    
    class Meta:
        verbose_name = "Contrato"
        verbose_name_plural = "Contratos"

    def __str__(self):
        return self.funcionario.__str__()
