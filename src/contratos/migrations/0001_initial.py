# Generated by Django 3.0.8 on 2020-10-18 20:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('funcionarios', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='TipoContrato',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=20, verbose_name='Nombre del tipo de Contrato')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')),
            ],
            options={
                'verbose_name': 'Tipo de contrato',
                'verbose_name_plural': 'Tipos de contratos',
            },
        ),
        migrations.CreateModel(
            name='Contrato',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateField(verbose_name='Fecha de inicio contrato')),
                ('fecha_termino', models.DateField(verbose_name='Fecha de término de contrato')),
                ('fecha_induccion', models.DateField(blank=True, null=True, verbose_name='Fecha de inducción')),
                ('corresponde_induccion', models.BooleanField(default=True, verbose_name='Le Corresponde Inducción')),
                ('vigente', models.BooleanField(default=True, verbose_name='Contrato Vigente')),
                ('permanente', models.BooleanField(default=True, verbose_name='Permanente')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')),
                ('funcionario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contrato_funcionario', to='funcionarios.Funcionario', verbose_name='Funcionario')),
                ('tipo_contrato', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contratos.TipoContrato', verbose_name='Tipo de Contrato')),
            ],
            options={
                'verbose_name': 'Contrato',
                'verbose_name_plural': 'Contratos',
            },
        ),
    ]
