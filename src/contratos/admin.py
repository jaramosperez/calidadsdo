from django.contrib import admin
from .models import Contrato, TipoContrato


class ContratoAdmin(admin.ModelAdmin):
    """
    Clase para usar el Modelo Ambito en el DjangoAdmin.
    """


class TipoContratoAdmin(admin.ModelAdmin):
    """
    Clase para usar el modelo de tipo de contrato en el Admin
    """


admin.site.register(Contrato, ContratoAdmin)
admin.site.register(TipoContrato, TipoContratoAdmin)
