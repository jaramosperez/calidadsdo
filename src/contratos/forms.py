from django import forms
from .models import Contrato


class ContratoForm(forms.ModelForm):
    class Meta:
        model = Contrato
        fields = ['fecha_inicio', 'fecha_termino', 'fecha_induccion', 'tipo_contrato', 'corresponde_induccion', 'vigente', 'permanente']
        widgets = {
            'fecha_inicio': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'fecha_termino': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'fecha_induccion': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'tipo_contrato': forms.Select(attrs={'class': 'ui small selection dropdown'}),
            'corresponde_induccion': forms.CheckboxInput(attrs={'class': 'ui checkbox'}),
            'vigente': forms.CheckboxInput(attrs={'class': 'ui checkbox'}),
            'permanente': forms.CheckboxInput(attrs={'class': 'ui checkbox'}),
        }


class ContratoUpdateForm(forms.ModelForm):
    class Meta:
        model = Contrato
        fields = ['fecha_inicio', 'fecha_termino', 'fecha_induccion', 'tipo_contrato', 'corresponde_induccion', 'vigente', 'permanente']
        widgets = {
            'fecha_inicio': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'fecha_termino': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'fecha_induccion': forms.DateInput(format=('%d/%m/%Y'), attrs={'class': 'ui small calendar', 'id': 'calendario', 'type': 'date'}),
            'tipo_contrato': forms.Select(attrs={'class': 'ui small selection dropdown'}),
            'corresponde_induccion': forms.CheckboxInput(attrs={'class': 'ui checkbox'}),
            'vigente': forms.CheckboxInput(attrs={'class': 'ui checkbox'}),
            'permanente': forms.CheckboxInput(attrs={'class': 'ui checkbox'}),
        }