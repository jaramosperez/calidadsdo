from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .models import Contrato
from .forms import ContratoForm, ContratoUpdateForm
from funcionarios.models import Funcionario, FuncionarioManager
import datetime


# LISTAR LAS AUSENCIAS.
@method_decorator(login_required(), name='dispatch')
class ContratoList(ListView):

    def get_queryset(self):
        return Contrato.objects.listar_inducciones()

# DETALLE DE AUSENCIA.
@method_decorator(login_required(), name='dispatch')
class ContratoDetail(DetailView):
    model = Contrato

# CREAR UNA AUSENCIA PARA UN FUNCIONARIO.
@method_decorator(login_required(), name='dispatch')
class ContratoCreate(CreateView):
    model = Contrato
    form_class = ContratoForm
    success_url = reverse_lazy('contratos:contratos')

    # IDENTIFICAR AL FUNCIONARIO AL QUE SE LE ASIGNARA EL CONTRATO.
    def form_valid(self, form):
        funcionario_id = self.kwargs['funcionario_id']
        form.instance.funcionario = Funcionario.objects.get(id=funcionario_id)
        return super(ContratoCreate, self).form_valid(form)

    # OBTENER LOS DATOS PARA EL FUNCIONARIO SELECCIONADO.
    def get_context_data(self, **kwargs):
        context = super(ContratoCreate, self).get_context_data(**kwargs)
        funcionario_id = self.kwargs['funcionario_id']
        funcionario = Funcionario.objects.get(id=funcionario_id)
        context['funcionario'] = funcionario

        nombre_completo = funcionario.nombres + ' ' + funcionario.primer_apellido
        if funcionario.segundo_apellido != '':
            nombre_completo = nombre_completo + ' ' + funcionario.segundo_apellido
        context['nombre_completo'] = nombre_completo
        return context

# ACTUALIZAR LOS DATOS DE UNA AUSENCIA.
@method_decorator(login_required(), name='dispatch')
class ContratoUpdate(UpdateView):
    model = Contrato
    form_class = ContratoUpdateForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('contratos:actualizar', args=[self.object.id]) + '?ok'

# BORRAR UNA AUSENCIA.
@method_decorator(login_required(), name='dispatch')
class ContratoDelete(DeleteView):
    model = Contrato
    success_url = reverse_lazy('contratos:contratos')
