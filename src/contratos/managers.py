from django.db import models


class ContratoManager(models.Manager):
    """ Manager para el modelo Ambito """

    def listar_inducciones(self):
        
        return self.filter(corresponde_induccion=True)