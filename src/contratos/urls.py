from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import ContratoList, ContratoDetail, ContratoCreate, ContratoUpdate, ContratoDelete

contratos_patterns = ([
    path('', ContratoList.as_view(), name='contratos'),
    path('<int:pk>/', ContratoDetail.as_view(), name="contrato"),
    path('crear/<funcionario_id>', ContratoCreate.as_view(), name="crear"),
    path('actualizar/<int:pk>/', ContratoUpdate.as_view(), name="actualizar"),
    path('borrar/<int:pk>/', ContratoDelete.as_view(), name="borrar"),
], 'contratos')
