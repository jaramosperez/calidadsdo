from django.db import models


class AmbitoManager(models.Manager):
    """ Manager para el modelo Ambito """

    def listar_ambitos(self):
        return self.all()