from django.contrib import admin
from .models import Profesion


class ProfesionAdmin(admin.ModelAdmin):
    """
    Clase para usar el Modelo Ambito en el DjangoAdmin.
    """
    list_display = ('nombre',)
    readonly_fields = ('created', 'updated')


admin.site.register(Profesion, ProfesionAdmin)
