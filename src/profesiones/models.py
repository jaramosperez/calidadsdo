from django.db import models


class Profesion(models.Model):
    nombre = models.CharField(max_length=20, verbose_name="Nombre de Profesión")
    categoria = models.CharField(max_length=1, verbose_name="Categoria")
    cod_sanitario = models.BooleanField(default=True, 
        verbose_name="Requiere código sanitario")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name='Fecha de creación'
        )
    updated = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de edición'
        )
    
    class Meta:
        verbose_name = "Profesión"
        verbose_name_plural = "Profesiones"
    
    def __str__(self):
        return self.nombre

