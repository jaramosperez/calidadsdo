from django import forms
from .models import Componente


class ComponenteFormUpdate(forms.ModelForm):
    class Meta:
        model = Componente
        fields = ['nombre', 'numero', 'definicion']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'ui small input'}),
            'numero': forms.TextInput(attrs={'class': 'ui small input'}),
            'definicion': forms.TextInput(attrs={'class': 'ui small input'}),
        }