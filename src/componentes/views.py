from .models import Componente
from .forms import ComponenteFormUpdate
from caracteristicas.models import Caracteristica
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


@method_decorator(login_required(), name='dispatch')
class ComponenteList(ListView):
    model = Componente


@method_decorator(login_required(), name='dispatch')
class ComponenteDetail(DetailView):
    model = Componente

    def get_context_data(self, **kwargs):
        context = super(ComponenteDetail, self).get_context_data(**kwargs)
        caracteristica_listado = Caracteristica.objects.filter(
            componente_id=self.object.id
            )
        context['caracteristica_listado'] = caracteristica_listado

        return context


@method_decorator(login_required(), name='dispatch')
class ComponenteUpdate(UpdateView):
    model = Componente
    form_class = ComponenteFormUpdate
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('componentes:componentes')
