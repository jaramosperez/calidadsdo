from django.contrib import admin
from .models import Componente


class ComponenteAdmin(admin.ModelAdmin):
    """
    Clase para usar el Modelo Ambito en el DjangoAdmin.
    """
    list_display = ('nombre', 'numero', 'definicion')
    readonly_fields = ('created', 'updated')


admin.site.register(Componente, ComponenteAdmin)
