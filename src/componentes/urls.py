from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required
from .views import ComponenteList, ComponenteDetail
from .views import ComponenteUpdate

componentes_patterns = ([
    path('', login_required(ComponenteList.as_view()), name='componentes'),
    path(
        '<int:pk>/<slug:slug>/',
        login_required(ComponenteDetail.as_view()),
        name='componente'),
    path(
        'actualizar/<int:pk>/<slug:slug>/',
        login_required(ComponenteUpdate.as_view()), name='actualizar'
        ),
], 'componentes')
