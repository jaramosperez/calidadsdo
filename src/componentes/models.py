from django.db import models
from ambitos.models import Ambito
from ckeditor.fields import RichTextField


# Create your models here.
class Componente(models.Model):
    nombre = models.CharField(
        max_length=20,
        verbose_name="Nombre del componente"
        )
    numero = models.SmallIntegerField(
        verbose_name="Número de componente en el ámbito"
        )
    ambito = models.ForeignKey(
        Ambito, verbose_name="Nombre del ámbito",
        blank=True,
        null=True,
        on_delete=models.CASCADE
        )
    definicion = RichTextField(
        verbose_name="Definición del Componente"
        )
    created = models.DateTimeField(
        auto_now_add=True, verbose_name='Fecha de creación'
        )
    updated = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de edición'
        )

    class Meta:
        verbose_name = 'Componente'
        verbose_name_plural = 'Componentes'
        ordering = ['ambito', 'numero']

    def __str__(self):
        return self.nombre
